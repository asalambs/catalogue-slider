﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Widgets.CatalogueSlider.Helpers;
using System.Collections.Generic;
using Nop.Plugin.Widgets.CatalogueSlider.Services;
using System.Linq;
using Nop.Core.Caching;
using Nop.Services.Helpers;
using System.Threading.Tasks;
using Nop.Plugin.Widgets.CatalogueSlider.Models;
using Nop.Web.Framework.Components;
using Nop.Services.Media;
using Nop.Plugin.Widgets.CatalogueSlider.Factories;
using Nop.Plugin.Widgets.CatalogueSlider.Infrastructure.Cache;
using Nop.Services.Configuration;
using Nop.Services.Catalog;
using Nop.Core.Domain.Catalog;
using System;
using Nop.Core.Domain.Vendors;
using Nop.Core.Domain.Logging;
using Nop.Services.Logging;
using Nop.Web.Models.Catalog;
using Nop.Web.Framework.Infrastructure;
using Nop.Plugin.Widgets.CatalogueSlider.Domain;
using Nop.Services.Customers;

namespace Nop.Plugin.Widgets.CatalogueSlider.Components
{
    [ViewComponent(Name = "CatalogueSlider")]
    public class CatalogueSliderViewComponent : NopViewComponent
    {
        #region Fields
        private readonly IPictureService _pictureService;
        private readonly IWebHelper _webHelper;
        private readonly ISliderService _sliderService;
        private readonly ISliderModelFactory _sliderFactory;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly IWorkContext _workContext;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly ICategoryService _categoryService;
        private readonly ILogger _logger;
        private readonly IUserAgentHelper _userAgentHelper;
        private readonly ICustomerService _customerService;
        #endregion
        #region Ctor
        public CatalogueSliderViewComponent(
            IPictureService pictureService,
            IWebHelper webHelper,
            ISliderService sliderService,
            ISliderModelFactory sliderFactory,
            IStaticCacheManager staticCacheManager,
            IWorkContext workContext,
            ISettingService settingService,
            IStoreContext storeContext,
            ICategoryService categoryService,
            ILogger logger,
            IUserAgentHelper userAgentHelper,
            ICustomerService customerService
            )
        {
            _pictureService = pictureService;
            _webHelper = webHelper;
            _sliderService = sliderService;
            _sliderFactory = sliderFactory;
            _staticCacheManager = staticCacheManager;
            _workContext = workContext;
            _settingService = settingService;
            _storeContext = storeContext;
            _categoryService = categoryService;
            _logger = logger;
            _userAgentHelper = userAgentHelper;
            _customerService = customerService;
        }
        #endregion
        #region Utility
        private async Task<CatalogModel> GetEnityNameAndId(object additionalData)
        {
            CatalogModel catalogModel = new CatalogModel();
            var path = additionalData is CategoryModel categoryModel ? categoryModel.Name : "";
            if (string.IsNullOrEmpty(path))
            {
                path = additionalData is ManufacturerModel menufactureModel ? menufactureModel.Name : "";
                if (string.IsNullOrEmpty(path))
                {
                    path = additionalData is VendorModel vendorModel ? vendorModel.Name : "";
                    if (!string.IsNullOrEmpty(path))
                    {
                        var specifiVen = await _sliderFactory.GetVendor(new Vendor { Name = path });
                        catalogModel.EntityId = specifiVen.Count > 0 ? Convert.ToInt32(specifiVen.First().Id) : 0;
                        catalogModel.EntityName = "Vendor";
                    }
                }
                else
                {
                    var specifiMan = await _sliderFactory.GetMenufacture(new Manufacturer { Name = path });
                    catalogModel.EntityId = specifiMan.Count > 0 ? Convert.ToInt32(specifiMan.First().Id) : 0;
                    catalogModel.EntityName = "Manufacture";
                }
            }
            else
            {
                var specificCat = await _sliderFactory.GetCategory(new Category { Name = path });
                catalogModel.EntityId = specificCat.Count > 0 ? Convert.ToInt32(specificCat.First().Id) : 0;
                catalogModel.EntityName = "Category";
            }

            return catalogModel;
        }
        public async Task<SliderModel> PrepareSlider(Slider sliders, string widgetZone, bool isMobile, int widgetZoneId)
        {
            var cacheKey = _staticCacheManager.PrepareKeyForDefaultCache(SliderCache.PublicComponentKey,
                           widgetZone, isMobile,
                           (await _workContext.GetWorkingLanguageAsync()).Id);

            var sliderModel = await _staticCacheManager.GetAsync(cacheKey, async () =>
            {
                return new SliderModel
                {
                    Id = sliders.Id,
                    Items = _sliderService.GetSliderItemsBySliderIdAsync(sliders.Id).Result.OrderBy(o => o.DisplayOrder).Select(x => new SliderItemModel
                    {
                        SliderId = sliders.Id,
                        PictureUrl = isMobile ? GetUrl(x.MobilePictureId).Result : GetUrl(x.PictureId).Result,
                        PictureId = x.PictureId,
                        DisplayOrder = x.DisplayOrder,
                        MobilePictureId = x.MobilePictureId
                    }).ToList(),
                    WidgetZoneId = widgetZoneId,
                    IsActive = sliders.IsActive,
                    TimeInterval = sliders.TimeInterval < 5000 ? 5000 : sliders.TimeInterval,
                    StartTime = sliders.StartTime,
                    EndTime = sliders.EndTime
                };

            });

            return sliderModel != null ? sliderModel : new SliderModel();
        }
        public async Task<string> GetUrl(int pictureId)
        {
            var url = await _pictureService.GetPictureUrlAsync(pictureId, showDefaultPicture: false) ?? "";
            return url;
        }
        #endregion
        public async Task<IViewComponentResult> InvokeAsync(string widgetZone, object additionalData)
        {
            try
            {
                if(additionalData == null)
                    return Content("");

                var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
                var csSetting = await _settingService.LoadSettingAsync<CatalogueSliderSettings>(storeScope);
                var model = new SliderModel();
                if (csSetting.EnableSlider)
                {
                    CatalogModel catalogModel = await GetEnityNameAndId(additionalData);

                    if (catalogModel == null || catalogModel?.EntityId == 0)
                        return Content("");

                    if (!WidgetZoneHelper.TryGetWidgetZoneId(widgetZone, out int widgetZoneId))
                        return Content("");

                    var entity = await _sliderService.PublicEntityAsync(widgetZoneId, catalogModel.EntityName, catalogModel.EntityId);
                    if(entity == null)
                        return Content("");

                    var customer = await _workContext.GetCurrentCustomerAsync();
                    //var customerRoleId = await _customerService.GetCustomerRoleIdsAsync(customer);
                    var currentStoreId = (await _storeContext.GetCurrentStoreAsync()).Id;

                    var sliders = await _sliderService.PublicSlideAsync(entity.SliderId, currentStoreId);
                    if (sliders == null)
                        return Content("");
                    var isMobileDevice = _userAgentHelper.IsMobileDevice();
                    model = await PrepareSlider(sliders, widgetZone, isMobileDevice, widgetZoneId);
                }
                return View("~/Plugins/Widgets.CatalogueSlider/Views/Default.cshtml", model);
            }
            catch (Exception ex)
            {
                await _logger.InsertLogAsync(LogLevel.Error, "Catalogue Slider. Error create from server side component", ex.ToString());
                return Content("");
            }
            
        }
        
    }
}
