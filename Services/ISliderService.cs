﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Plugin.Widgets.CatalogueSlider.Domain;
using Nop.Plugin.Widgets.CatalogueSlider.Models;

namespace Nop.Plugin.Widgets.CatalogueSlider.Services
{
    public interface ISliderService
    {
        #region Slider
        Task<IPagedList<Slider>> GetAllSliderAsync(int storeId = 0,
           int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false, bool? overrideActivated = null);
        Task<IList<Slider>> GetAllSlidersAsync(SliderSearchModel slider = null);
        Task<Slider> GetSliderByIdAsync(int sliderId);
        //Task<Slider> GetSliderByWidgetZoneIdAsync(int widgetZoneId, string entityName, int specificId, int id = 0);
        //Task<Slider> PublicSlideShowAsync(int widgetZoneId, string entityName, int specificId);
        Task<IList<Slider>> GetSliderByIdsAsync(int[] sliderIds);
        Task InsertSliderAsync(Slider slider);

        Task UpdateSliderAsync(Slider slider);

        Task DeleteSliderAsync(Slider slider);
        Task DeleteSlidersAsync(IList<Slider> sliders);
        Task<Slider> PublicSlideAsync(int slierId, int storeId);
        #endregion

        #region Slider items

        Task<IList<SliderItem>> GetSliderItemsBySliderIdAsync(int sliderId);
        Task<IList<EntityZone>> GetSliderEntityWithZoneBySliderIdAsync(int sliderId);
        Task<SliderItem> GetSliderItemByIdAsync(int sliderItemId);

        Task InsertSliderItemAsync(SliderItem sliderItem);

        Task UpdateSliderItemAsync(SliderItem sliderItem);
        Task DeleteSliderItemAsync(SliderItem sliderItem);
        Task DeleteSliderItemssAsync(IList<SliderItem> sliderItems);
        //Task<IPagedList<SliderItem>> GetSliderItemsBySliderIdAsync(int sliderId, int pageIndex = 0,
        //   int pageSize = int.MaxValue);
        Task<IList<SliderItem>> GetSliderItemsByIdsAsync(int[] sliderItemsIds);
        #endregion

        #region SliderEntity
        //Task<SliderEntity> GetSliderEntityUniqueCheckAsync(IList<int> widgetZoneIds, List<string> entityName, IList<int> entityIds, int sliderId = 0);
        Task<SliderEntity> GetSliderEntityUniqueCheckAsync(string entityName, int entityId);
        Task<SliderEntity> PublicSlideEntityCheckAsync(int widgetZoneId, string entityName, int entityId);
        Task InsertSliderEntityAsync(SliderEntity sliderEntity);
        Task<SliderEntity> PublicEntityAsync(int widgetZoneId, string entityName, int entityId);
        Task UpdateSliderEntityAsync(SliderEntity slider);
        Task<IList<SliderEntity>> GetSliderEntityBySliderIdAsync(int sliderId);
        Task DeleteSliderEntityItemAsync(SliderEntity sliderEntity);
        Task DeleteSliderEntityAsync(IList<SliderEntity> sliderEntities);
        #endregion
        #region SliderZone
        Task<IList<SliderZone>> GetSliderZoneBySliderEntityIdAsync(int sliderEntityId);
        Task InsertSliderZoneAsync(SliderZone sliderZone);
        Task DeleteSliderZoneAsync(SliderZone sliderZone);
        Task DeleteSliderZoneAsync(IList<SliderZone> sliderZones);
        #endregion
    }
}