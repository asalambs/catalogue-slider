﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Stores;
using Nop.Core.Events;
using Nop.Data;
using Nop.Plugin.Widgets.CatalogueSlider.Domain;
using Nop.Plugin.Widgets.CatalogueSlider.Models;
using Nop.Services.Security;
using Nop.Services.Stores;

namespace Nop.Plugin.Widgets.CatalogueSlider.Services
{
    public class SliderService : ISliderService
    {
        #region Fields
        protected readonly IRepository<Slider> _sliderRepository;
        protected readonly IRepository<SliderItem> _sliderItemRepository;
        protected readonly IRepository<SliderEntity> _sliderEntityRepository;
        protected readonly IRepository<SliderZone> _sliderZoneRepository;
        private readonly IAclService _aclService;
        protected readonly IStoreMappingService _storeMappingService;
        private readonly IWorkContext _workContext;
        #endregion

        #region Constants

        #endregion

        #region ctor

        public SliderService(
            IRepository<Slider> sliderRepository,
            IRepository<SliderItem> sliderItemRepository,
            IRepository<SliderEntity> sliderEntityRepository,
            IRepository<SliderZone> sliderZoneRepository,
            IAclService aclService,
            IStoreMappingService storeMappingService,
            IWorkContext workContext)
        {
            _sliderRepository = sliderRepository;
            _sliderItemRepository = sliderItemRepository;
            _sliderEntityRepository = sliderEntityRepository;
            _sliderZoneRepository = sliderZoneRepository;
            _aclService = aclService;
            _storeMappingService = storeMappingService;
            _workContext = workContext;
        }
        #endregion


        #region Slider
        public async Task<IPagedList<Slider>> GetAllSliderAsync(int storeId = 0,
           int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false, bool? overrideActivated = null)
        {
            var slider = await _sliderRepository.GetAllAsync(async query =>
            {
                //if (!showHidden)
                //    query = query.Where(c => c.IsActive);
                if (overrideActivated.HasValue)
                    query = query.Where(c => c.IsActive == overrideActivated.Value);

                //apply store mapping constraints
                query = await _storeMappingService.ApplyStoreMapping(query, storeId);

                //apply ACL constraints
                if (!showHidden)
                {
                    var customer = await _workContext.GetCurrentCustomerAsync();
                    query = await _aclService.ApplyAcl(query, customer);
                }

                //if (!string.IsNullOrWhiteSpace(categoryName))
                //    query = query.Where(c => c.Name.Contains(categoryName));

                //query = query.Where(c => !c.Deleted);

                return query;
            });

            //sort categories
            //var sortedCategories = await SortCategoriesForTreeAsync(unsortedCategories);

            //paging
            return new PagedList<Slider>(slider, pageIndex, pageSize);
        }
        public async Task<IList<Slider>> GetAllSlidersAsync(SliderSearchModel slider)
        {
            var sliders = (await _sliderRepository.GetAllAsync(query =>
            {
                return from ca in query
                       orderby ca.Id descending
                       select ca;
            })).ToAsyncEnumerable();

            return await sliders.ToListAsync<Slider>(); 
        }
    
        public async Task<Slider> GetSliderByIdAsync(int sliderId)
        {
           return await _sliderRepository.GetByIdAsync(sliderId);
        }
        //public async Task<Slider> GetSliderByWidgetZoneIdAsync(int widgetZoneId, string entityName, int specificId, int id = 0)
        //{
        //    var sliders = (await _sliderRepository.GetAllAsync(query =>
        //    {
        //        if(id == 0)
        //        return from ca in query
        //               //where ca.WidgetZoneId == widgetZoneId && ca.EntityId == specificId
        //               select ca;
        //        else
        //        {
        //            return from ca in query
        //                   //where ca.WidgetZoneId == widgetZoneId && ca.EntityId == specificId && ca.Id != id
        //                   select ca;
        //        }
        //    })).ToAsyncEnumerable();

        //    return await sliders.FirstOrDefaultAsync();
        //}

        public async Task<SliderEntity> PublicEntityAsync(int widgetZoneId, string entityName, int entityId)
        {
            var entity = from se in _sliderEntityRepository.Table
            join sz in _sliderZoneRepository.Table
            on se.Id equals sz.SliderEntityId
            where sz.WidgetZoneId == widgetZoneId && se.EntityName == entityName && se.EntityId == entityId
            select se;

            return await entity.FirstOrDefaultAsync();
        }
        public async Task<Slider> PublicSlideAsync(int slierId, int storeId)
        {
            var sliders = (await _sliderRepository.GetAllAsync(async query =>
            {
                query = from ca in query
                        where ca.Id == slierId && 
                         (!ca.StartTime.HasValue || ca.StartTime <= DateTime.UtcNow) &&
                          (!ca.EndTime.HasValue || ca.EndTime >= DateTime.UtcNow) && ca.IsActive == true
                       select ca;

                query = await _storeMappingService.ApplyStoreMapping(query, storeId);
                var customer = await _workContext.GetCurrentCustomerAsync();
                query = await _aclService.ApplyAcl(query, customer);

                return query;
            })).ToAsyncEnumerable();
            var slider = await sliders.FirstOrDefaultAsync();

            return slider;
        }

        //public async Task<List<Slider>> PublicSlideShowAsync(int widgetZoneId, string entityName, int entityId)
        //{
        //    var sliders = (await _sliderRepository.GetAllAsync(query =>
        //    {
        //            return from ca in query
        //                   where ca.IsActive == true
        //                   select ca;

        //    })).ToAsyncEnumerable();
        //    var entity = (await _sliderEntityRepository.GetAllAsync(query =>
        //    {
        //        return from ca in query
        //               where ca.WidgetZoneId == widgetZoneId && ca.EntityId == entityId
        //               select ca;

        //    })).ToAsyncEnumerable();

        //    var sliderEntity = await sliders.Join(entity, s => s.Id, e => e.SliderId, (s, e) => new 
        //    {
        //        Id = s.Id,
        //        TimeInterval = s.TimeInterval,
        //        StartTime = s.StartTime,
        //        EndTime = s.EndTime,
        //        StoreId = s.StoreId,
        //        CustomerRoleIds = s.CustomerRoleIds,
        //        WidgetZoneId = e.WidgetZoneId,
        //        EntityId = e.EntityId,
        //        EntityName = e.EntityName
        //    }).ToListAsync();

        //    return await sliderEntity.ToList();
        //}
        public virtual async Task<IList<Slider>> GetSliderByIdsAsync(int[] sliderIds)
        {
            return await _sliderRepository.GetByIdsAsync(sliderIds);
        }
        public async Task InsertSliderAsync(Slider slider)
        {
            await _sliderRepository.InsertAsync(slider);
        }
        public async Task UpdateSliderAsync(Slider slider)
        {
            await _sliderRepository.UpdateAsync(slider);
        }
        public async Task DeleteSliderAsync(Slider slider)
        {
            await _sliderRepository.DeleteAsync(slider);
        }
        public async Task DeleteSlidersAsync(IList<Slider> sliders)
        {
            if (sliders == null)
                throw new ArgumentNullException(nameof(sliders));

            foreach (var slider in sliders)
                await DeleteSliderAsync(slider);
        }

        public async Task<IList<SliderItem>> GetSliderItemsBySliderIdAsync(int sliderId)
        {
            var sliderItemsById = _sliderItemRepository.Table.Where(x => x.SliderId == sliderId);
            return await sliderItemsById.ToListAsync<SliderItem>();
        }
        #endregion

        #region Slider items
        public async Task<SliderItem> GetSliderItemByIdAsync(int sliderItemId)
        {
            return await _sliderItemRepository.GetByIdAsync(sliderItemId);
        }
        public async Task<IList<SliderItem>> GetSliderItemsByIdsAsync(int[] sliderItemsIds)
        {
            return await _sliderItemRepository.GetByIdsAsync(sliderItemsIds);
        }
        public async Task InsertSliderItemAsync(SliderItem sliderItem)
        {
            await _sliderItemRepository.InsertAsync(sliderItem);
        }

        public async Task UpdateSliderItemAsync(SliderItem sliderItem)
        {
            await _sliderItemRepository.UpdateAsync(sliderItem);
        }

        public async Task DeleteSliderItemAsync(SliderItem sliderItem)
        {
            await _sliderItemRepository.DeleteAsync(sliderItem);
        }

        public async Task DeleteSliderItemssAsync(IList<SliderItem> sliderItems)
        {
            if (sliderItems == null)
                throw new ArgumentNullException(nameof(sliderItems));

            foreach (var sliderItem in sliderItems)
                await DeleteSliderItemAsync(sliderItem);
        }
        public async Task<SliderEntity> GetSliderEntityUniqueCheckAsync(string entityName, int entityId)
        {
            var sliderEntity = await (from se in _sliderEntityRepository.Table
                                      where se.EntityId == entityId && se.EntityName == entityName 
                                      select se).FirstOrDefaultAsync();

            return sliderEntity;
            //
            //var sliderEntity = (await _sliderEntityRepository.GetAllAsync(query =>
            //{
            //    return from ca in query
            //           where widgetZoneIds.Contains(ca.WidgetZoneId) && entityIds.Contains(ca.EntityId)
            //           && entityName.Contains(ca.EntityName) 
            //           select ca;

            //})).ToAsyncEnumerable();

            //return await sliderEntity.FirstOrDefaultAsync();
        }
        //public async Task<SliderEntity> GetSliderEntityUniqueCheckAsync(IList<int> widgetZoneIds, List<string> entityName, IList<int> entityIds, int sliderId = 0)
        //{
        //    var sliderEntity = await (from se in _sliderEntityRepository.Table
        //                 join sz in _sliderZoneRepository.Table
        //                 on se.Id equals sz.SliderEntityId
        //                 where widgetZoneIds.Contains(sz.WidgetZoneId) && entityIds.Contains(se.EntityId)
        //                 select se).FirstOrDefaultAsync();

        //    return sliderEntity;
        //    //
        //    //var sliderEntity = (await _sliderEntityRepository.GetAllAsync(query =>
        //    //{
        //    //    return from ca in query
        //    //           where widgetZoneIds.Contains(ca.WidgetZoneId) && entityIds.Contains(ca.EntityId)
        //    //           && entityName.Contains(ca.EntityName) 
        //    //           select ca;

        //    //})).ToAsyncEnumerable();

        //    //return await sliderEntity.FirstOrDefaultAsync();
        //}

        public async Task<SliderEntity> PublicSlideEntityCheckAsync(int widgetZoneId, string entityName, int entityId)
        {
            //if (!isEdit)
                return await (from se in _sliderEntityRepository.Table
                              join sz in _sliderZoneRepository.Table
                              on se.Id equals sz.SliderEntityId
                              where sz.WidgetZoneId == widgetZoneId && se.EntityId == entityId && se.EntityName == entityName
                              select se).FirstOrDefaultAsync();
            //else
            //    return await (from se in _sliderEntityRepository.Table
            //                  join sz in _sliderZoneRepository.Table
            //                  on se.Id equals sz.SliderEntityId
            //                  where sz.WidgetZoneId == widgetZoneId && se.EntityId == entityId && se.EntityName == entityName
            //                  && se.SliderId != sliderId
            //                  select se).FirstOrDefaultAsync();
            //var sliders = (await _sliderEntityRepository.GetAllAsync(query =>
            //{
            //    if(sliderId == 0)
            //        return from ca in query
            //            where ca.WidgetZoneId == widgetZoneId && ca.EntityId == entityId && ca.EntityName == entityName
            //            select ca;
            //    else
            //        return from ca in query
            //               where ca.WidgetZoneId == widgetZoneId && ca.EntityId == entityId 
            //               && ca.EntityName == entityName && ca.SliderId != sliderId
            //               select ca;

            //})).ToAsyncEnumerable();

            //return await sliders.FirstOrDefaultAsync();
        }
        public async Task InsertSliderEntityAsync(SliderEntity sliderEntity)
        {
            await _sliderEntityRepository.InsertAsync(sliderEntity);
        }

        public async Task UpdateSliderEntityAsync(SliderEntity sliderEntity)
        {
            await _sliderEntityRepository.UpdateAsync(sliderEntity);
        }

        public async Task DeleteSliderEntityItemAsync(SliderEntity sliderEntity)
        {
            await _sliderEntityRepository.DeleteAsync(sliderEntity);
        }

        public async Task DeleteSliderEntityAsync(IList<SliderEntity> sliderEntities)
        {
            if (sliderEntities == null)
                throw new ArgumentNullException(nameof(sliderEntities));
            else if(sliderEntities.Count == 0)
                throw new ArgumentNullException(nameof(sliderEntities));

            foreach (var se in sliderEntities)
                await DeleteSliderEntityItemAsync(se);

        }
        public async Task<IList<SliderEntity>> GetSliderEntityBySliderIdAsync(int sliderId)
        {
            return await _sliderEntityRepository.Table.Where(x => x.SliderId == sliderId).ToListAsync();
        }
        public async Task<IList<EntityZone>> GetSliderEntityWithZoneBySliderIdAsync(int sliderId)
        {
            return await (from se in _sliderEntityRepository.Table
                    join sz in _sliderZoneRepository.Table
                    on se.Id equals sz.SliderEntityId
                    where se.SliderId == sliderId
                    select new EntityZone { EntityId = se.EntityId, EntityName = se.EntityName, Id = se.Id, SliderEntityId = sz.Id, 
                        SliderId = se.SliderId, WidgetZoneId = sz.WidgetZoneId }).ToListAsync();
            //return await _sliderEntityRepository.Table.Where(x => x.SliderId == sliderId).ToListAsync();
        }
        public async Task<IList<SliderZone>> GetSliderZoneBySliderEntityIdAsync(int sliderEntityId)
        {
            return await _sliderZoneRepository.Table.Where(x => x.SliderEntityId == sliderEntityId).ToListAsync();
        }

        public async Task InsertSliderZoneAsync(SliderZone sliderZone)
        {
            await _sliderZoneRepository.InsertAsync(sliderZone);
        }
        public async Task DeleteSliderZoneAsync(SliderZone sliderZone)
        {
            await _sliderZoneRepository.DeleteAsync(sliderZone);
        }
        public async Task DeleteSliderZoneAsync(IList<SliderZone> sliderZones)
        {
            if (sliderZones == null)
                throw new ArgumentNullException(nameof(sliderZones));
            else if (sliderZones.Count == 0)
                throw new ArgumentNullException(nameof(sliderZones));

            foreach (var sz in sliderZones)
                await DeleteSliderZoneAsync(sz);

        }
        #endregion
    }
}
