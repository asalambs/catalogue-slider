﻿function CatalogueSlider(id, iv, sc) {
    var $this = $('#CS' + id);
    var $group = $this.find('.CatalogueSlider_slide_group');
    var $slides = $this.find('.CatalogueSlider_slide');
    //var $slides = $this.find('#CS' + id);
    var currentIndex = 0;
    var timeout;

    function move(newIndex) {
        var animateLeft, slideLeft;

        advance();

        if ($group.is(':animated') || currentIndex === newIndex) {
            return;
        }

        if (newIndex > currentIndex) {
            slideLeft = '100%';
            animateLeft = '-100%';
        } else {
            slideLeft = '-100%';
            animateLeft = '100%';
        }

        $slides.eq(newIndex).css({
            display: 'block',
            left: slideLeft
        });
        $group.animate({
            left: animateLeft
        }, function () {
            $slides.eq(currentIndex).css({
                display: 'none'
            });
            $slides.eq(newIndex).css({
                left: 0
            });
            $group.css({
                left: 0
            });
            currentIndex = newIndex;
        });
    }

    function advance() {
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            if (currentIndex < ($slides.length - 1)) {
                move(currentIndex + 1);
            } else {
                move(0);
            }
        }, iv);
    }

    $('#CatalogueSlider_next_btn' + id).on('click', function () {
        if (currentIndex < ($slides.length - 1)) {
            move(currentIndex + 1);
        } else {
            move(0);
        }
    });

    $('#CatalogueSlider_previous_btn' + id).on('click', function () {
        if (currentIndex !== 0) {
            move(currentIndex - 1);
        } else {
            move(sc - 1);
        }
    });

    advance();
}