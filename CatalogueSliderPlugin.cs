﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Routing;
using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Plugin.Widgets.CatalogueSlider.Domain;
using Nop.Plugin.Widgets.CatalogueSlider.Helpers;
using Nop.Plugin.Widgets.CatalogueSlider.Services;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Plugins;
using Nop.Services.Security;
using Nop.Web.Framework.Infrastructure;
using Nop.Web.Framework.Menu;

namespace Nop.Plugin.Widgets.CatalogueSlider
{
    /// <summary>
    /// PLugin
    /// </summary>
    public class CatalogueSliderPlugin : BasePlugin, IWidgetPlugin, IAdminMenuPlugin
    {
        #region Fields
        private readonly ILocalizationService _localizationService;
        private readonly IPictureService _pictureService;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly INopFileProvider _fileProvider;

        private readonly IPermissionService _permissionService;
        private readonly ISliderService _sliderService;
        private readonly IStoreContext _storeContext;
        #endregion
        #region Ctor
        public CatalogueSliderPlugin(ILocalizationService localizationService,
            IPictureService pictureService,
            ISettingService settingService,
            IWebHelper webHelper,
            INopFileProvider fileProvider, IPermissionService permissionService, ISliderService sliderService,
            IStoreContext storeContext)
        {                                 
            _localizationService = localizationService;
            _pictureService = pictureService;
            _settingService = settingService;
            _webHelper = webHelper;
            _fileProvider = fileProvider;
            _permissionService = permissionService;
            _sliderService = sliderService;
            _storeContext = storeContext;
        }
        #endregion

        protected async Task CreateSampleDataAsync()
        {
            var sliderSetting = new CatalogueSliderSettings()
            {
                EnableSlider = true
            };
            await _settingService.SaveSettingAsync(sliderSetting);
        }
        public Task<IList<string>> GetWidgetZonesAsync()
        {
            IList<string> widgetZonesName = WidgetZoneHelper.GetCustomWidgetZones();
            return Task.FromResult<IList<string>>( widgetZonesName );
        }

        /// <summary>
        /// Gets a configuration page URL
        /// </summary>
        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/CatalogueSlider/Configure";
        }

        /// <summary>
        /// Gets a name of a view component for displaying widget
        /// </summary>
        /// <param name="widgetZone">Name of the widget zone</param>
        /// <returns>View component name</returns>
        public string GetWidgetViewComponentName(string widgetZone)
        {
            return "CatalogueSlider";
        }

        /// <summary>
        /// Install plugin
        /// </summary>
        /// <returns>A task that represents the asynchronous operation</returns>
        public override async Task InstallAsync()
        {
            var settings = new CatalogueSliderSettings
            {
                EnableSlider = true
                //TimeInterval = 5000
            };
            await _settingService.SaveSettingAsync(settings);

            await _localizationService.AddLocaleResourceAsync(new Dictionary<string, string>
            {
                ["Plugins.Widgets.CatalogueSlider"] = "All Your Slider",
                ["Plugins.Widgets.CatalogueSlider.Configuration"] = "Configuration",
                ["Plugins.Widgets.CatalogueSlider.Configuration.Fields.EnableSlider"] = "Enable Slider",
                ["Plugins.Widgets.CatalogueSlider.Configuration.Fields.EnableSlider.Hint"] = "Enable/Disable Your Slider",
                ["Plugins.Widgets.CatalogueSlider.Configuration.Fields.TimeInterval"] = "Time Interval",
                ["Plugins.Widgets.CatalogueSlider.Info"] = "Info",
                ["Plugins.Widgets.CatalogueSlider.Items"] = "Items",
                ["Plugins.Widgets.CatalogueSlider.AddNew"] = "Add New",
                ["Plugins.Widgets.CatalogueSlider.BackToList"] = "Back To List",
                ["Plugins.Widgets.CatalogueSlider.EditDetails"] = "Edit Slider",
                ["Admin.Widgets.CatalogueSlider.Sliders.Saved"] = "Slider Saved Successfully",
                ["Plugins.Widgets.CatalogueSlider.Sliders.Updated"] = "Slider Updated Successfully",
                ["Plugins.Widgets.CatalogueSlider.Pictures.AddButton"] = "Upload a file",
                ["Plugins.Widgets.CatalogueSlider.Pictures.Alert.AddNew"] = "Upload picture first.",
                ["Plugins.Widgets.CatalogueSlider.Pictures.Alert.PictureAdd"] = "git.txt has an invalid extension. Valid extension(s): bmp, gif, jpeg, jpg, jpe, jfif, pjpeg, pjp, png, tiff, tif, webp.",
                ["Plugins.Widgets.CatalogueSlider.SliderItem.AddNewItem"] = "Add a new item",
                ["Plugins.Widgets.CatalogueSlider.Fields.Picture"] = "Picture",
                ["Plugins.Widgets.CatalogueSlider.Fields.MobilePicture"] = "Picture for Mobile",
                ["Plugins.Widgets.CatalogueSlider.Fields.Name"] = "Name",
                ["Plugins.Widgets.CatalogueSlider.Fields.IsActive"] = "Is Active",
                ["Plugins.Widgets.CatalogueSlider.Fields.IsActive.Hint"] = "This Slider Active Inactive Define Here",
                ["Plugins.Widgets.CatalogueSlider.Fields.WidgetZone"] = "Widget Zone",
                ["Plugins.Widgets.CatalogueSlider.Fields.WidgetZonePublicName"] = "Widget Zone Name",
                ["Plugins.Widgets.CatalogueSlider.Fields.WidgetZone.Hint"] = "Where you want to place the slider",
                ["Plugins.Widgets.CatalogueSlider.Fields.TimeInterval"] = "Time Interval",
                ["Plugins.Widgets.CatalogueSlider.Fields.TimeInterval.Hint"] = "Time Interval In Second, Minimum 5 Seconds",
                ["Plugins.Widgets.CatalogueSlider.Fields.PictureId"] = "Picture",
                ["Plugins.Widgets.CatalogueSlider.Fields.PictureId.Hint"] = "Set an image for slider",
                ["Plugins.Widgets.CatalogueSlider.Fields.DisplayOrder"] = "Display Order",
                ["Plugins.Widgets.CatalogueSlider.Fields.DisplayOrder.Hint"] = "Display order of images",
                ["Plugins.Widgets.CatalogueSlider.Fields.Name.Required"] = "The name field is required.",
                ["Plugins.Widgets.CatalogueSlider.Fields.Name.Length"] = "The name is not more than 200 char long!",
                ["Plugins.Widgets.CatalogueSlider.Fields.TimeInterval.Wrong"] = "Time Interval is a number",
                ["Plugins.Widgets.CatalogueSlider.NotValid"] = "Fields are not valid",
                ["Plugins.Widgets.CatalogueSlider.Fields.CatalogId"] = "Catalog Name",
                ["Plugins.Widgets.CatalogueSlider.Fields.SpecificationName"] = "Specific Place",
                ["Plugins.Widgets.CatalogueSlider.Fields.CatalogType"] = "Catalog Type",
                ["Plugins.Widgets.CatalogueSlider.Configuration.Fields.AllWidgetZones"] = "All Widget Zones",
                ["Plugins.Widgets.CatalogueSlider.Fields.EntityName.Required"] = "Entity name is required",
                ["Plugins.Widgets.CatalogueSlider.Fields.StartTime"] = "Start Time",
                ["Plugins.Widgets.CatalogueSlider.Fields.StartTime.Hint"] = "Slide starting time",
                ["Plugins.Widgets.CatalogueSlider.Fields.EndTime"] = "End Time",
                ["Plugins.Widgets.CatalogueSlider.Fields.EndTime.Hint"] = "Slide ending time",
                ["Plugins.Widgets.CatalogueSlider.Fields.Required"] = "Some required field is empty!",
                ["Plugins.Widgets.CatalogueSlider.Fields.AclCustomerRoles"] = "Customer roles",
                ["Plugins.Widgets.CatalogueSlider.Fields.AclCustomerRoles.Hint"] = "Choose one or several customer roles i.e. administrators, vendors, guests, who will be able to see this product in catalog. If you don't need this option just leave this field empty. In order to use this functionality, you have to disable the following setting: Configuration > Settings > Catalog > Ignore ACL rules (sitewide).",
                ["Plugins.Widgets.CatalogueSlider.Fields.LimitedToStores"] = "Limited to stores",
                ["Plugins.Widgets.CatalogueSlider.Fields.LimitedToStores.Hint"] = "Option to limit this product to a certain store. If you have multiple stores, choose one or several from the list. If you don't use this option just leave this field empty. In order to use this functionality, you have to disable the following setting: Configuration > Catalog settings > Ignore 'limit per store' rules (sitewide).",
                ["Plugins.Widgets.CatalogueSlider.SliderItem.EditItem"] = "Edit Item",
                ["Plugins.Widgets.CatalogueSlider.Fields.SearchActiveId"] = "Is Active",
                ["Plugins.Widgets.CatalogueSlider.List.SearchPublished.All"] = "All",
                ["Plugins.Widgets.CatalogueSlider.List.SearchPublished.ActiveOnly"] = "Active",
                ["Plugins.Widgets.CatalogueSlider.List.SearchPublished.InActiveOnly"] = "Inactive",
                ["Plugins.Widgets.CatalogueSlider.Fields.Invalid.StartTime"] = "Start time is invalide",
                ["Plugins.Widgets.CatalogueSlider.Fields.Invalid.EndTime"] = "End time is invalide",
                ["Plugins.Widgets.CatalogueSlider.Fields.Invalid.StartTime_EndTime"] = "Start time not grater than end time!",
                ["Plugins.Widgets.CatalogueSlider.Fields.EntityId.Required"] = "Category name is required!",
                ["Plugins.Widgets.CatalogueSlider.Fields.widgetzoneid.required"] = "Widget zone is required!"
            }); 

            await base.InstallAsync();
            await CreateSampleDataAsync();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        /// <returns>A task that represents the asynchronous operation</returns>
        public override async Task UninstallAsync()
        {
            //settings
            await _settingService.DeleteSettingAsync<CatalogueSliderSettings>();

            //locales
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Configuration");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Configuration.Fields.EnableSlider");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Configuration.Fields.TimeInterval");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Info");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Item");

            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Fields.Name");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Fields.IsActive");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Fields.IsActive.Hint");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.AddNew");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.BackToList");
            
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Fields.WidgetZone");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Fields.WidgetZone.Hint");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.EditDetails");

            await _localizationService.DeleteLocaleResourcesAsync("Admin.Widgets.CatalogueSlider.Sliders.Saved");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Sliders.Updated");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Pictures.AddButton");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Pictures.Alert.AddNew");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Pictures.Alert.PictureAdd");
            
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.SliderItem.AddNewItem");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Fields.WidgetZonePublicName");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Fields.TimeInterval");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Fields.TimeInterval.Hint");

            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Fields.PictureId");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Fields.PictureId.Hint");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Fields.Picture");

            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Fields.DisplayOrder");
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.CatalogueSlider.Fields.DisplayOrder.Hint");

            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.Name.Required");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.Name.Length");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.TimeInterval.Wrong");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.NotValid");

            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.CatalogId");
            //await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.ManufacturerId");
            //await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.VendorId");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.SpecificationName");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.CatalogType");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Configuration.Fields.AllWidgetZones");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.EntityName.Required");

            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.StartTime");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.StartTime.Hint");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.EndTime");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.EndTime.Hint");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.Required");

            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.AclCustomerRoles");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.AclCustomerRoles.Hint");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.LimitedToStores");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.LimitedToStores.Hint");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.MobilePicture");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.MobilePicture.Hint");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.SliderItem.EditItem");

            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.SearchActiveId");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.List.SearchPublished.All");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.List.SearchPublished.ActiveOnly");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.List.SearchPublished.InActiveOnly");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.Invalid.StartTime");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.Invalid.EndTime");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.Invalid.StartTime_EndTime");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.EntityId.Required");
            await _localizationService.DeleteLocaleResourceAsync("Plugins.widgets.catalogueslider.fields.widgetzoneid.required");

            await base.UninstallAsync();
        }
        public Task ManageSiteMapAsync(SiteMapNode rootNode)
        {
            ManageSiteMap(rootNode);

            return Task.CompletedTask;
        }

        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var menuItem = new SiteMapNode()
            {
                Title = "Catalogue Slider",
                Visible = true,
                IconClass = "nav-icon far fa-dot-circle",
                RouteValues = new RouteValueDictionary() { { "area", "Admin" } },
            };

            var configureItem = new SiteMapNode()
            {
                Title = "Setting",
                ControllerName = "CatalogueSlider",
                ActionName = "Configure",
                Visible = true,
                IconClass = "nav-icon far fa-circle",
                SystemName = "CatalogueSliderSettings",
                RouteValues = new RouteValueDictionary() { { "area", "Admin" } },
            };
            var thanaAreaPostCodeItem = new SiteMapNode()
            {
                Title = "All Your Slider",
                ControllerName = "CatalogueSlider",
                ActionName = "List",
                Visible = true,
                IconClass = "nav-icon far fa-circle",
                SystemName = "CatalogueSlider",
                RouteValues = new RouteValueDictionary() { { "area", "Admin" } },
            };
            

            menuItem.ChildNodes.Add(configureItem);
            menuItem.ChildNodes.Add(thanaAreaPostCodeItem);
           

            //var pluginNode = rootNode.ChildNodes.FirstOrDefault(x => x.SystemName == "nopStation");
            //if (pluginNode != null)
            //    pluginNode.ChildNodes.Add(menuItem);
            //else
            //{
                var nopStation = new SiteMapNode()
                {
                    Visible = true,
                    Title = "nopStation",
                    Url = "",
                    SystemName = "nopStation",
                    IconClass = "nav-icon fas fa-book"
                };
                rootNode.ChildNodes.Add(nopStation);
                nopStation.ChildNodes.Add(menuItem);
            //}
        }
        /// <summary>
        /// Gets a value indicating whether to hide this plugin on the widget list page in the admin area
        /// </summary>
        public bool HideInWidgetList => false;
    }
}