﻿using FluentMigrator;
using FluentMigrator.Infrastructure;
using Nop.Data.Migrations;
using Nop.Plugin.Widgets.CatalogueSlider.Domain;

namespace Nop.Plugin.Widgets.CatalogueSlider.Data
{
    [SkipMigrationOnUpdate]
    [NopMigration("2021/06/03 12:24:16:2551777", "Widgets.CatalogueSlider base schema")]
    public class SchemaMigration : AutoReversingMigration
    {
        private readonly IMigrationManager _migrationManager;

        public SchemaMigration(IMigrationManager migrationManager)
        {
            _migrationManager = migrationManager;
        }

        public override void Up()
        {
            _migrationManager.BuildTable<Slider>(Create);
            _migrationManager.BuildTable<SliderItem>(Create);
            _migrationManager.BuildTable<SliderEntity>(Create);
            _migrationManager.BuildTable<SliderZone>(Create);
        }
        
    }
}
