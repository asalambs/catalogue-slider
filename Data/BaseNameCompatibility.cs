﻿using Nop.Data.Mapping;
using Nop.Plugin.Widgets.CatalogueSlider.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nop.Plugin.Widgets.CatalogueSlider.Data
{
    public class BaseNameCompatibility : INameCompatibility
    {
        public Dictionary<Type, string> TableNames => new Dictionary<Type, string>
        {
            { typeof(Slider), "NS_Catalogue_Slider" },
            { typeof(SliderItem), "NS_Catalogue_SliderItem" },
            { typeof(SliderEntity), "NS_Catalogue_SliderEntity" },
            { typeof(SliderZone), "NS_Catalogue_SliderZone" }
        };
        public Dictionary<(Type, string), string> ColumnName => new Dictionary<(Type, string), string>
        {

        };

    }
}
