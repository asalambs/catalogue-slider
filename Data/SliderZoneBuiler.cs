﻿using FluentMigrator.Builders.Create.Table;
using Nop.Data.Extensions;
using Nop.Data.Mapping.Builders;
using Nop.Plugin.Widgets.CatalogueSlider.Domain;

namespace Nop.Plugin.Widgets.CatalogueSlider.Data
{
    public class SliderZoneBuiler : NopEntityBuilder<SliderZone>
    {
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(SliderZone.SliderEntityId)).AsInt32().ForeignKey<SliderEntity>();
        }
    }
}
