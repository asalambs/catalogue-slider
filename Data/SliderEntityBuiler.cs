﻿using FluentMigrator.Builders.Create.Table;
using Nop.Data.Extensions;
using Nop.Data.Mapping.Builders;
using Nop.Plugin.Widgets.CatalogueSlider.Domain;

namespace Nop.Plugin.Widgets.CatalogueSlider.Data
{
    public class SliderEntityBuilder : NopEntityBuilder<SliderEntity>
    {
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(SliderEntity.SliderId)).AsInt32().ForeignKey<Slider>();
        }
    }
}
