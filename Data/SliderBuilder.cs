﻿using FluentMigrator.Builders.Create.Table;
using Nop.Data.Mapping;
using Nop.Data.Mapping.Builders;
using Nop.Plugin.Widgets.CatalogueSlider.Domain;

namespace Nop.Plugin.Widgets.CatalogueSlider.Data
{
    public class SliderBuilder : NopEntityBuilder<Slider>
    {
        public override void MapEntity(CreateTableExpressionBuilder table)
        {
            table
                .WithColumn(nameof(Slider.Name)).AsString(200).NotNullable();

        }
    }
}