﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Configuration;
using System.Collections.Generic;

namespace Nop.Plugin.Widgets.CatalogueSlider
{
    public class CatalogueSliderSettings : ISettings
    {
        public CatalogueSliderSettings()
        {
            //AllWidgetZones = new List<SelectListItem>();
            //SelectedWidgetZonesIds = new List<int>();
            //SelectedWidgetZones = new List<string>();
        }
        public bool EnableSlider { get; set; }
        //public IList<SelectListItem> AllWidgetZones { get; set; }
        //public List<int> SelectedWidgetZonesIds { get; set; }
        //public List<string> SelectedWidgetZones { get; set; }
    }
}