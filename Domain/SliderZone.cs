﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CatalogueSlider.Domain
{
    public class SliderZone : BaseEntity
    {
        public SliderZone()
        {
             
        }
        public int WidgetZoneId { get; set; }
        public int SliderEntityId { get; set; }
    }
}
