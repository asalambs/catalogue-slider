﻿using Nop.Core;
using Nop.Core.Domain.Localization;

namespace Nop.Plugin.Widgets.CatalogueSlider.Domain
{
    public class SliderItem : BaseEntity//, ILocalizedEntity
    {
        public int PictureId { get; set; }

        public int SliderId { get; set; }

        public int DisplayOrder { get; set; }
        public int MobilePictureId { get; set; }
    }
}
