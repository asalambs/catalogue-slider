﻿using Nop.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CatalogueSlider.Domain
{
    public class SliderEntity : BaseEntity
    {
        public SliderEntity()
        {
             
        }
        public string EntityName { get; set; }
        public int EntityId { get; set; }
        public int SliderId { get; set; }
    }
}
