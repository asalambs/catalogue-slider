﻿using Nop.Core;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Stores;
using System;
using System.Collections.Generic;

namespace Nop.Plugin.Widgets.CatalogueSlider.Domain
{
    public class Slider : BaseEntity, IAclSupported, IStoreMappingSupported
    {
        public Slider()
        {
        }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public int TimeInterval { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        //public string StoreIds { get; set; }
        //public string CustomerRoleIds { get; set; }
        public bool SubjectToAcl { get; set; }
        public bool LimitedToStores { get; set; }
    }
}
