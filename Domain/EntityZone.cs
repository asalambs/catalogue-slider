﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CatalogueSlider.Domain
{
    public class EntityZone: SliderEntity
    {
        public int WidgetZoneId { get; set; }
        public int SliderEntityId { get; set; }
    }
}
