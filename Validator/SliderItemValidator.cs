﻿using FluentValidation;
using Nop.Plugin.Widgets.CatalogueSlider.Models;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;

namespace Nop.Plugin.Widgets.CatalogueSlider.Validator
{
    public class SliderItemValidator : BaseNopValidator<SliderItemModel>
    {
        public SliderItemValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.PictureId).GreaterThan(0).WithMessage(localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.Required").Result);
            RuleFor(x => x.MobilePictureId).GreaterThan(0).WithMessage(localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.Required").Result);
            RuleFor(x => x.DisplayOrder).GreaterThan(-1).WithMessage(localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.TimeInterval.Required").Result);
            RuleFor(x => x.SliderId).GreaterThan(0).WithMessage(localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.WidgetZoneId.Required").Result);
        }
    }
}
