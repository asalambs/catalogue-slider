﻿using Nop.Plugin.Widgets.CatalogueSlider.Models;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using FluentValidation;
using System;

namespace Nop.Plugin.Widgets.CatalogueSlider.Validator
{
    public class SliderValidator : BaseNopValidator<SliderModel>
    {
        public SliderValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.Name.Required").Result);
            RuleFor(x => x.Name).Length(1, 200).WithMessage(localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.Name.Length").Result);
            //RuleFor(x => x.EntityStringIds).NotEmpty().WithMessage(localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.EntityId.Required").Result);
            RuleFor(x => x.TimeInterval).GreaterThan(4).WithMessage(localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.TimeInterval.Required").Result);

            RuleFor(x => x.StartTime).Must((x, context) =>
            {
                if (!x.StartTime.HasValue)
                    return true;

                if (x.StartTime <= DateTime.UtcNow)
                {
                    return false;
                }
                
                return true;
            }).WithMessage(localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.Invalid.StartTime").Result);
            RuleFor(x => x.EndTime).Must((x, context) =>
            {
                if (!x.EndTime.HasValue || !x.StartTime.HasValue)
                    return true;

                if (x.EndTime <= DateTime.UtcNow)
                {
                    return false;
                }

                return true;
            }).WithMessage(localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.Invalid.EndTime").Result);


            RuleFor(x => x.StartTime).Must((x, context) =>
            {
                if (!x.StartTime.HasValue)
                    return true;

                if (x.StartTime.HasValue && x.EndTime.HasValue)
                {
                    if (x.StartTime > x.EndTime)
                    {
                        return false;
                    }
                }
                return true;
            }).WithMessage(localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.Invalid.StartTime_EndTime").Result);

            RuleFor(x => x.WidgetZoneIds).NotEmpty().WithMessage(localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.Required").Result);
            RuleFor(x => x.WidgetZoneIds).Must(x => x.Count > 0).WithMessage(localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.WidgetZoneId.Required").Result);
            RuleFor(x => x.EntityStringIds).Must((x, context) =>
            {
                if (string.IsNullOrEmpty(x.EntityStringIds)) return false;
                var entity = Common.GetCatalogModel(x.EntityStringIds);

                if (entity.Count == 0) return false;
                return true;
            }).WithMessage(localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.Fields.Invalid.Category").Result);


            //if (!ModelState.IsValid)
            //{
            //    var erroneousFields = ModelState.Where(ms => ms.Value.Errors.Any())
            //                        .Select(x => x.Key).ToList();
            //    if (erroneousFields.Count == 1 && erroneousFields[0] == "EntityIds") ModelState.Clear();
            //}

        }
    }
}
