﻿using Microsoft.AspNetCore.Mvc;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Web.Areas.Admin.Controllers;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Plugin.Widgets.CatalogueSlider.Domain;
using Nop.Plugin.Widgets.CatalogueSlider.Services;
using Nop.Services.Media;
using Nop.Web.Framework.Mvc;
using System;
using Nop.Services.Configuration;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Core;
using System.Linq;
using Nop.Services.Stores;
using Nop.Core.Caching;
using System.Threading.Tasks;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework;
using Nop.Plugin.Widgets.CatalogueSlider.Models;
using Nop.Web.Areas.Admin.Factories;
using Nop.Plugin.Widgets.CatalogueSlider.Factories;
using System.Collections.Generic;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Services.Logging;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Logging;
using Nop.Plugin.Widgets.CatalogueSlider.Infrastructure.Cache;
using Nop.Services.Catalog;
using Nop.Plugin.Widgets.CatalogueSlider.Helpers;
using Nop.Web.Framework.Infrastructure;
using System.Reflection;
using static Nop.Plugin.Widgets.CatalogueSlider.Models.SliderModel;
using Nop.Services.Customers;
using Nop.Web.Framework.Factories;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Nop.Plugin.Widgets.CatalogueSlider.Controllers
{
    [AuthorizeAdmin]
    [Area(AreaNames.Admin)]
    [AutoValidateAntiforgeryToken]
    public class CatalogueSliderController : BasePluginController
    {
        #region Fields
        private readonly ISliderModelFactory _sliderModelFactory;
        private readonly ISliderService _sliderService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly IPermissionService _permissionService;
        private readonly IBaseAdminModelFactory _baseAdminModelFactory;
        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IPictureService _pictureService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly ILogger _logger;
        private readonly IStaticCacheManager _staticCacheManager;
        private readonly ICategoryService _categoryService;
        private readonly ICustomerService _customerService;
        private readonly IStoreService _storeService;
        private readonly IAclService _aclService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IAclSupportedModelFactory _aclSupportedModelFactory;
        private readonly IStoreMappingSupportedModelFactory _storeMappingSupportedModelFactory;
        #endregion

        #region Ctor

        public CatalogueSliderController(
            ISliderModelFactory sliderModelFactory,
            ISliderService sliderService, 
            ISettingService settingService,
            IStoreContext storeContext,
            IPermissionService permissionService,
            IBaseAdminModelFactory baseAdminModelFactory,
            ILocalizationService localizationService,
            INotificationService notificationService,
            IPictureService pictureService,
            ICustomerActivityService customerActivityService,
            ILogger logger,
            IStaticCacheManager staticCacheManager,
            ICategoryService categoryService,
            ICustomerService customerService,
            IStoreService storeService,
            IAclService aclService,
            IStoreMappingService storeMappingService,
            IAclSupportedModelFactory aclSupportedModelFactory,
            IStoreMappingSupportedModelFactory storeMappingSupportedModelFactory
            )
        {
            _sliderModelFactory = sliderModelFactory;
            _sliderService = sliderService;
            _settingService = settingService;
            _storeContext = storeContext;
            _permissionService = permissionService;
            _baseAdminModelFactory = baseAdminModelFactory;
            _localizationService = localizationService;
            _notificationService = notificationService;
            _pictureService = pictureService;
            _customerActivityService = customerActivityService;
            _logger = logger;
            _staticCacheManager = staticCacheManager;
            _categoryService = categoryService;
            _customerService = customerService;
            _storeService = storeService;
            _aclService = aclService;
            _storeMappingService = storeMappingService;
            _aclSupportedModelFactory = aclSupportedModelFactory;
            _storeMappingSupportedModelFactory = storeMappingSupportedModelFactory;
        }

        #endregion

        #region Utilities
        private IEnumerable<SelectListItem> GetWidgetFilterd(string[] filters)
        {
            //generate empty list
            var selectList = new List<SelectListItem>();
            //if (string.IsNullOrEmpty(filter)) return selectList;
            if (filters?.Length == 0) return selectList;
            foreach (string filter in filters)
            {
                CatalogEnum myEnum = (CatalogEnum)Enum.Parse(typeof(CatalogEnum), filter);
                var all = WidgetZoneHelper.GetCustomWidgetZoneSelectList().Where(w => w.Text.Contains(myEnum.ToString()));
                foreach (var s in all)
                {
                    //add elements in dropdown
                    selectList.Add(new SelectListItem
                    {
                        Value = s.Value.ToString(),
                        Text = s.Text.ToString()
                    });
                }
            }
            return selectList;
        }
        private IEnumerable<SelectListItem> GetCatalogFilterd(string[] filters)
        {
            IList<SelectListItem> selectList = new List<SelectListItem>();

            if (filters?.Length == 0) return selectList;
            foreach (string f in filters)
            {
                //var enumDisplayStatus = (CatalogEnum)filter;
                //CatalogEnum myEnum = (CatalogEnum)filter;

                CatalogEnum myEnum = (CatalogEnum)Enum.Parse(typeof(CatalogEnum), f);
                string filter = myEnum.ToString();
                try
                {
                    if (filter[0].ToString().ToLower() == "c")
                    {
                        //((List<SelectListItem>)selectList).AddRange(_sliderModelFactory.GetSelectCategory().Result);
                        var list = _sliderModelFactory.GetSelectCategory().Result;
                        foreach (var s in list)
                        {
                            //add elements in dropdown
                            selectList.Add(new SelectListItem
                            {
                                Value = "c-" + s.Value.ToString(),
                                Text = s.Text.ToString()
                            });
                        }
                    }
                    else if (filter[0].ToString().ToLower() == "m")
                    {
                        //((List<SelectListItem>)selectList).AddRange(_sliderModelFactory.GetSelectManufacture().Result);
                        var list = _sliderModelFactory.GetSelectManufacture().Result;
                        foreach (var s in list)
                        {
                            //add elements in dropdown
                            selectList.Add(new SelectListItem
                            {
                                Value = "m-" + s.Value.ToString(),
                                Text = s.Text.ToString()
                            });
                        }
                    }
                    else
                    {
                        //((List<SelectListItem>)selectList).AddRange(_sliderModelFactory.GetSelectVendor().Result);
                        var list = _sliderModelFactory.GetSelectVendor().Result;
                        foreach (var s in list)
                        {
                            selectList.Add(new SelectListItem
                            {
                                Value = "v-" + s.Value.ToString(),
                                Text = s.Text.ToString()
                            });
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
            
            return selectList;
        }
        protected async Task SaveSliderAclAsync(Slider slider, SliderModel model)
        {
            slider.SubjectToAcl = model.SelectedCustomerRoleIds.Any(); 
            await _sliderService.UpdateSliderAsync(slider);

            var existingAclRecords = await _aclService.GetAclRecordsAsync(slider);
            var allCustomerRoles = await _customerService.GetAllCustomerRolesAsync(true);
            foreach (var customerRole in allCustomerRoles)
            {
                if (model.SelectedCustomerRoleIds.Contains(customerRole.Id))
                {
                    //new role
                    if (existingAclRecords.Count(acl => acl.CustomerRoleId == customerRole.Id) == 0)
                        await _aclService.InsertAclRecordAsync(slider, customerRole.Id);
                }
                else
                {
                    //remove role
                    var aclRecordToDelete = existingAclRecords.FirstOrDefault(acl => acl.CustomerRoleId == customerRole.Id);
                    if (aclRecordToDelete != null)
                        await _aclService.DeleteAclRecordAsync(aclRecordToDelete);
                }
            }
        }
        protected async Task SaveStoreMappingsAsync(Slider slider, SliderModel model)
        {
            slider.LimitedToStores = model.SelectedStoreIds.Any();
            await _sliderService.UpdateSliderAsync(slider);

            var existingStoreMappings = await _storeMappingService.GetStoreMappingsAsync(slider);
            var allStores = await _storeService.GetAllStoresAsync();
            foreach (var store in allStores)
            {
                if (model.SelectedStoreIds.Contains(store.Id))
                {
                    //new store
                    if (existingStoreMappings.Count(sm => sm.StoreId == store.Id) == 0)
                        await _storeMappingService.InsertStoreMappingAsync(slider, store.Id);
                }
                else
                {
                    //remove store
                    var storeMappingToDelete = existingStoreMappings.FirstOrDefault(sm => sm.StoreId == store.Id);
                    if (storeMappingToDelete != null)
                        await _storeMappingService.DeleteStoreMappingAsync(storeMappingToDelete);
                }
            }
        }
        #endregion

        #region Methods
        #region Configuration
        public async Task<IActionResult> Configure()
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.AccessAdminPanel))
                return AccessDeniedView();

            var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var csSetting = await _settingService.LoadSettingAsync<CatalogueSliderSettings>(storeScope);

            var model = new ConfigurationModel();
            //model.AllWidgetZones = WidgetZone.GetAllWidgetZoneSelectList();
            model.EnableSlider = csSetting.EnableSlider;
            //model.SelectedWidgetZonesIds = csSetting.SelectedWidgetZonesIds;

            return View("~/Plugins/Widgets.CatalogueSlider/Areas/Admin/Views/CatalogueSlider/Configure.cshtml", model);
        }
        [HttpPost]
        public async Task<IActionResult> Configure(ConfigurationModel model)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();
            try
            {
                var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
                var catalogueSliderSettings = await _settingService.LoadSettingAsync<CatalogueSliderSettings>(storeScope);
                //catalogueSliderSettings = model.ToSettings(catalogueSliderSettings);
                
                catalogueSliderSettings.EnableSlider = model.EnableSlider;
                //catalogueSliderSettings.SelectedWidgetZonesIds = new List<int>(model.SelectedWidgetZonesIds);
                await _settingService.SaveSettingOverridablePerStoreAsync(catalogueSliderSettings, x => x.EnableSlider, model.EnableSlider_OverrideForStore, storeScope, false);
                //await _settingService.SaveSettingOverridablePerStoreAsync(catalogueSliderSettings, x => x.SelectedWidgetZonesIds, model.SelectedWidgetZonesIds_OverrideForStore, storeScope, false);
                //await _settingService.SaveSettingOverridablePerStoreAsync(catalogueSliderSettings, x => x.TimeInterval, model.TimeInterval_OverrideForStore, storeScope, false);

                await _settingService.ClearCacheAsync();

                _notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Plugins.Saved"));

            }
            catch (Exception ex)
            {
                _notificationService.ErrorNotification("Something goes wrong!");
                await _logger.InsertLogAsync(LogLevel.Error, "Catalogue Slider. Error configuration from server side", ex.ToString());
            }

            
            return RedirectToAction("Configure");
        }
        #endregion

        #region List
        public async Task<IActionResult> List()
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.AccessAdminPanel))
                return AccessDeniedView();
            var model = await _sliderModelFactory.PrepareSliderSearchModelAsync(new SliderSearchModel());
            return View(model);
        }
        [HttpPost]
        public virtual async Task<IActionResult> List(SliderSearchModel searchModel)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.AccessAdminPanel))
                return await AccessDeniedDataTablesJson();

            //prepare model
            var model = await _sliderModelFactory.PrepareSliderListModelAsync(searchModel);

            return Json(model);
        }
        
        
        #endregion

        #region Create / update / delete

        public async Task<IActionResult> Create()
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.AccessAdminPanel))
                return AccessDeniedView();

            var model = await _sliderModelFactory.PrepareModelForCreate(new SliderModel());
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public async Task<IActionResult> Create(SliderModel model, bool continueEditing)
        {
            var erroneousFields = new List<string>();
            try
            {
                var entity = Common.GetCatalogModel(model.EntityStringIds);
                erroneousFields = ModelState.Where(ms => ms.Value.Errors.Any())
                                    .Select(x => x.Key).ToList();
                if (erroneousFields.Count == 1 && erroneousFields[0] == "EntityIds") ModelState.Remove("EntityIds");
                

                if (ModelState.IsValid)
                {
                    var slider = model.ToEntity<Slider>();
                    var imeInterval = model.TimeInterval < 5
                            ? 5 * 1000 : model.TimeInterval * 1000;
                    slider.TimeInterval = imeInterval;
                    
                    await _sliderService.InsertSliderAsync(slider);

                    await _sliderModelFactory.SaveSliderEntity(entity, model.WidgetZoneIds, slider.Id);

                    await SaveSliderAclAsync(slider, model);
                    await SaveStoreMappingsAsync(slider, model);

                    await _staticCacheManager.RemoveByPrefixAsync(SliderCache.PublicComponenPrefixCacheKey);
                    _notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Widgets.CatalogueSlider.Sliders.Saved"));

                    return continueEditing
                            ? RedirectToAction("Edit", new { id = slider.Id })
                            : RedirectToAction("List");
                }
            }
            catch (Exception ex)
            {
                await _logger.InsertLogAsync(LogLevel.Error, "Catalogue Slider. Error create from server side", ex.ToString());
            }
            var erroneousStrings = ModelState.Where(ms => ms.Value.Errors.Any())
                                           .Select(x => x.Value).ToList();
            if (erroneousStrings.Count > 0 && erroneousStrings.First().Errors.Count > 0)
            _notificationService.ErrorNotification(erroneousStrings.First().Errors.First().ErrorMessage);
            model = await _sliderModelFactory.PrepareModelForCreate(model);
            return View(model);
        }

        public async Task<IActionResult> Edit(int id)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.AccessAdminPanel))
                return AccessDeniedView();

            var slider = await _sliderService.GetSliderByIdAsync(id);
            if (slider == null)
                return RedirectToAction("List");

            var model = await _sliderModelFactory.PrepareSliderModelAsync(slider);

            model.SliderItemSearchModel = await _sliderModelFactory.PrepareSliderItemSearchModelAsync(new SliderItemSearchModel());
            //call from SliderItemCreate
            //so path must asign
            return View("~/Plugins/Widgets.CatalogueSlider/Areas/Admin/Views/CatalogueSlider/Edit.cshtml", model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual async Task<IActionResult> Edit(SliderModel model, bool continueEditing)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.AccessAdminPanel))
                return AccessDeniedView();
            Slider slider = new Slider();
            try
            {
                slider = await _sliderService.GetSliderByIdAsync(model.Id);
                if (slider == null)
                    return RedirectToAction("List");

                    var entity = Common.GetCatalogModel(model.EntityStringIds);
                    if (!ModelState.IsValid)
                    {
                        var erroneousFields = ModelState.Where(ms => ms.Value.Errors.Any())
                                            .Select(x => x.Key).ToList();
                    if (erroneousFields.Count == 1 && erroneousFields[0] == "EntityIds") ModelState.Remove("EntityIds");
                }
                
                if (ModelState.IsValid)
                {
                    slider = model.ToEntity(slider);
                    var imeInterval = model.TimeInterval < 5
                                ? 5 * 1000 : model.TimeInterval * 1000;
                    slider.TimeInterval = imeInterval;
                    await _sliderService.UpdateSliderAsync(slider);

                    await SaveSliderAclAsync(slider, model);
                    await SaveStoreMappingsAsync(slider, model);

                    IList<SliderEntity> sliderEntities = await _sliderService.GetSliderEntityBySliderIdAsync(slider.Id);
                    await _sliderService.DeleteSliderEntityAsync(sliderEntities);
                    
                    await _sliderModelFactory.SaveSliderEntity(entity, model.WidgetZoneIds, slider.Id);
                    await _staticCacheManager.RemoveByPrefixAsync(SliderCache.PublicComponenPrefixCacheKey);
                    _notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.Sliders.Updated"));

                return continueEditing
                    ? RedirectToAction("Edit", new { id = model.Id })
                    : RedirectToAction("List");
                }
            }
            catch (Exception ex)
            {
                _notificationService.ErrorNotification("Something goes wrong!");
                await _logger.InsertLogAsync(LogLevel.Error, "Catalogue Slider. Error edit from server side", ex.ToString());
            }
            slider = await _sliderService.GetSliderByIdAsync(model.Id);
            model = await _sliderModelFactory.PrepareModelForEdit(slider);
            //_notificationService.ErrorNotification(await _localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.NotValid"));
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(ICollection<int> selectedIds)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageAttributes))
                return AccessDeniedView();
            bool result = false;
            try
            {
                if (selectedIds != null)
                {
                    var sliders = await _sliderService.GetSliderByIdsAsync(selectedIds.ToArray());
                    await _sliderService.DeleteSlidersAsync(sliders);

                    foreach (var slider in sliders)
                    {
                        //activity log
                        await _customerActivityService.InsertActivityAsync("DeleteSlider",
                            string.Format(await _localizationService.GetResourceAsync("ActivityLog.DeleteSlider"), slider.Name), slider);
                    }
                    result = true;
                    await _staticCacheManager.RemoveByPrefixAsync(SliderCache.PublicComponenPrefixCacheKey);
                }
                else
                {
                    _notificationService.WarningNotification("No one is selected!");
                }
            }
            catch (Exception ex)
            {
                _notificationService.ErrorNotification("Something goes wrong!");
                await _logger.InsertLogAsync(LogLevel.Error, "Catalogue Slider. Error delete from server side", ex.ToString());
            }
            

            return Json(new { Result = result });
        }

        [HttpPost]
        public async Task<IActionResult> SliderItems(SliderItemSearchModel searchModel)
        {
            var model = await _sliderModelFactory.PrepareSliderItemListModelAsync(searchModel);
            return Json(model);
        }
        public IActionResult SliderItemCreate(int id)
        {
            var model = new SliderItemModel();
            model.SliderId = id;
            return View("~/Plugins/Widgets.CatalogueSlider/Areas/Admin/Views/CatalogueSlider/SliderItemCreatePopup.cshtml", model);
        }
        [HttpPost]
        public virtual async Task<IActionResult> SliderItemCreate(SliderItemModel model)
        {
            //bool result = false;
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.AccessAdminPanel))
                return await AccessDeniedDataTablesJson();
            try
            {
                if (ModelState.IsValid)
                {
                    var sliderItem = await _sliderModelFactory.PrepareSliderItemModelFromSliderItemAsync(model);
                    await _sliderService.InsertSliderItemAsync(sliderItem);
                    await _staticCacheManager.RemoveByPrefixAsync(SliderCache.PublicComponenPrefixCacheKey);
                    ViewBag.RefreshPage = true;
                    return await Edit(model.SliderId);
                }
            }
            catch (Exception ex)
            {
                _notificationService.ErrorNotification("Something goes wrong!");
                await _logger.InsertLogAsync(LogLevel.Error, "Catalogue Slider. Error delete from server side", ex.ToString());
            }
            ViewBag.RefreshPage = false;
            return View("~/Plugins/Widgets.CatalogueSlider/Areas/Admin/Views/CatalogueSlider/SliderItemCreatePopup.cshtml", model);
        }
        public async Task<IActionResult> SliderItemEdit(int id)
        {
            var model = await _sliderModelFactory.PrepareSliderItemModelAsync(id);
            return View("~/Plugins/Widgets.CatalogueSlider/Areas/Admin/Views/CatalogueSlider/SliderItemEditPopup.cshtml", model);
        }
        [HttpPost]
        public async Task<IActionResult> SliderItemEdit(SliderItemModel model)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.AccessAdminPanel))
                return await AccessDeniedDataTablesJson();

            SliderItem oldModel = new SliderItem();
            try
            {
                oldModel = await _sliderService.GetSliderItemByIdAsync(model.Id);
                if (oldModel != null)
                {
                    var item = model.ToEntity<SliderItem>();
                    if (ModelState.IsValid)
                    {
                        await _sliderService.UpdateSliderItemAsync(item);
                        await _staticCacheManager.RemoveByPrefixAsync(SliderCache.PublicComponenPrefixCacheKey);
                        ViewBag.RefreshPage = true;
                        return await Edit(item.SliderId);
                    }
                }
                else
                {
                    _notificationService.WarningNotification("Data not found!");
                }
            }
            catch (Exception ex)
            {
                _notificationService.ErrorNotification("Something goes wrong!");
                await _logger.InsertLogAsync(LogLevel.Error, "Catalogue Slider. Error Slider Item Edit from server side", ex.ToString());
            }

            ViewBag.RefreshPage = false;
            return View("~/Plugins/Widgets.CatalogueSlider/Areas/Admin/Views/CatalogueSlider/SliderItemEditPopup.cshtml", model);
        }
        [HttpPost]
        public virtual async Task<IActionResult> SliderItemDelete(int id)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.AccessAdminPanel))
                return await AccessDeniedDataTablesJson();

            try
            {
                var sliderItem = await _sliderService.GetSliderItemByIdAsync(id);
                await _sliderService.DeleteSliderItemAsync(sliderItem);
                await _staticCacheManager.RemoveByPrefixAsync(SliderCache.PublicComponenPrefixCacheKey);
            }
            catch (Exception ex)
            {
                _notificationService.ErrorNotification("Something goes wrong!");
                await _logger.InsertLogAsync(LogLevel.Error, "Catalogue Slider. Error Slider Item Delete from server side", ex.ToString());
            }
            
            return new NullJsonResult();
        }

        public JsonResult GetWidgetWithFilter(string[] filters)
        {
            var widgetList = GetWidgetFilterd(filters);
            return Json(widgetList);
        }
        public JsonResult GetCatalogs(string[] filters)
        {
            var catList = GetCatalogFilterd(filters);
            return Json(catList);
        }

        #endregion

        #endregion
    }
}