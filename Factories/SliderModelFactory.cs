﻿using System;
using System.Collections.Generic;
using Nop.Core;
using Nop.Plugin.Widgets.CatalogueSlider.Services;
using Nop.Plugin.Widgets.CatalogueSlider.Domain;
using Nop.Plugin.Widgets.CatalogueSlider.Models;
using Nop.Services.Localization;
using Nop.Services.Media;
//using Nop.Plugin.NopStation.Core.Services;
using System.Threading.Tasks;
using Nop.Services.Security;
using Nop.Web.Framework.Models.Extensions;
using System.Linq;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Plugin.Widgets.CatalogueSlider.Helpers;
using Nop.Services.Catalog;
using Nop.Core.Domain.Catalog;
using Nop.Services.Vendors;
using Nop.Core.Domain.Vendors;
using Nop.Services.Configuration;
using Nop.Web.Framework.Factories;

namespace Nop.Plugin.Widgets.CatalogueSlider.Factories
{
    public class SliderModelFactory : ISliderModelFactory
    {
        #region field
        private readonly ISliderService _sliderService;
        private readonly IPictureService _pictureService;
        private readonly ILocalizationService _localizationService;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufactureService;
        private readonly IVendorService _vendorService;
        private readonly IStoreContext _storeContext;
        private readonly ISettingService _settingService;
        private readonly IAclSupportedModelFactory _aclSupportedModelFactory;
        private readonly IStoreMappingSupportedModelFactory _storeMappingSupportedModelFactory;
        #endregion
        #region ctrl
        public SliderModelFactory(
            ISliderService sliderService,
            IPictureService pictureService,
            ILocalizationService localizationService,
            ICategoryService categoryService,
            IManufacturerService manufactureService,
            IVendorService vendorService,
            IStoreContext storeContext,
            ISettingService settingService,
            IAclSupportedModelFactory aclSupportedModelFactory,
            IStoreMappingSupportedModelFactory storeMappingSupportedModelFactory
            )
        {   
            _sliderService = sliderService;
            _pictureService = pictureService;
            _localizationService = localizationService;
            _categoryService = categoryService;
            _manufactureService = manufactureService;
            _vendorService = vendorService;
            _storeContext = storeContext;
            _settingService = settingService;
            _aclSupportedModelFactory = aclSupportedModelFactory;
            _storeMappingSupportedModelFactory = storeMappingSupportedModelFactory;
        }
        #endregion
        #region method
        public async Task<IList<SelectListItem>> PrepareCustomWidgetZonesAsync(bool withSpecialDefaultItem = false)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            var availableWidgetZones = WidgetZoneHelper.GetCustomWidgetZoneSelectList();
            //var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            //var csSetting = await _settingService.LoadSettingAsync<CatalogueSliderSettings>(storeScope);
            //var availableWidgetZones = WidgetZone.GetWidgetZoneSelectList(csSetting.SelectedWidgetZonesIds);
            foreach (var zone in availableWidgetZones)
            {
                items.Add(zone);
            }

            if (withSpecialDefaultItem)
                items.Insert(0, new SelectListItem()
                {
                    Text = await _localizationService.GetResourceAsync("Admin.Common.All"),
                    Value = "0"
                });

            return await Task.Run(() => items);
        }
        public async Task<SliderSearchModel> PrepareSliderSearchModelAsync(SliderSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            searchModel.AvailableActiveOptions.Add(new SelectListItem
            {
                Value = "0",
                Text = await _localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.List.SearchPublished.All")
            });
            searchModel.AvailableActiveOptions.Add(new SelectListItem
            {
                Value = "1",
                Text = await _localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.List.SearchPublished.ActiveOnly")
            });
            searchModel.AvailableActiveOptions.Add(new SelectListItem
            {
                Value = "2",
                Text = await _localizationService.GetResourceAsync("Plugins.Widgets.CatalogueSlider.List.SearchPublished.InActiveOnly")
            });

            //prepare page parameters
            searchModel.SetGridPageSize();

            return await Task.FromResult(searchModel);
        }
        public async Task<SliderListModel> PrepareSliderListModelAsync(SliderSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));
            //get sliders
            var sliders = await _sliderService.GetAllSliderAsync(
                pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize,
                overrideActivated: searchModel.SearchActiveId == 0 ? null : (bool?)(searchModel.SearchActiveId == 1));

            //prepare grid model
            var model = await new SliderListModel().PrepareToGridAsync(searchModel, sliders, () =>
            {
                return sliders.SelectAwait(async slider =>
                {
                    //fill in model values from the entity
                    var sliderModel = slider.ToModel<SliderModel>();
                    sliderModel.TimeIntervalString = (slider.TimeInterval / 1000) + " Sec";
                    sliderModel.TimeInterval = slider.TimeInterval / 1000;
                    return sliderModel;
                });
            });

            return model;
        }
        public async Task<SliderItemSearchModel> PrepareSliderItemSearchModelAsync(SliderItemSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //prepare page parameters
            searchModel.SetGridPageSize();

            return await Task.FromResult(searchModel);
        }
        public async Task<SliderModel> PrepareSliderModelAsync(Slider slider, bool continuing = false)
        {
            slider.TimeInterval = slider.TimeInterval / 1000;
            
            var model = slider.ToModel<SliderModel>();
            model.WidgetZoneItems = new List<SelectListItem>();
            model.CatalogTypeList = Common.EnumToSelectedList();

            var sliderEntity = await _sliderService.GetSliderEntityWithZoneBySliderIdAsync(slider.Id);
            List<string> entityNames = sliderEntity.Select(s => s.EntityName).ToList();
            foreach (string s in entityNames)
            {
                model.CatalogTypeIds.Add(EnumHelper.GetEnum<CatalogEnum>(s));
            }
            model.EntityIds = sliderEntity.Select(s => s.EntityId).ToList();

            model.EntityStringIds = string.Empty;
            foreach (var eni in sliderEntity)
            {
                model.EntityStringIds += Common.GetEntityCharFromName(eni.EntityName) + "-" + eni.EntityId + ",";
            }

            model.WidgetZoneIds = sliderEntity.Select(s => s.WidgetZoneId).Distinct().ToList();
            //prepare model customer roles
            await _aclSupportedModelFactory.PrepareModelCustomerRolesAsync(model, slider, false);
            //prepare model stores
            await _storeMappingSupportedModelFactory.PrepareModelStoresAsync(model, slider, false);
            return await Task.Run(() => model);
        }
        public async Task<SliderItemListModel>  PrepareSliderItemListModelAsync(SliderItemSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get checkout attributes
            searchModel.SetGridPageSize();
            var sliderAttributes = (await _sliderService.GetSliderItemsBySliderIdAsync(searchModel.SliderId)).ToPagedList(searchModel);

            ////prepare list model
            var model = await new SliderItemListModel().PrepareToGridAsync(searchModel, sliderAttributes, () =>
            {
                return sliderAttributes.SelectAwait(async item =>
                {
                    //fill in model values from the entity
                    var model = item.ToModel<SliderItemModel>();
                    model.PictureUrl = await _pictureService.GetPictureUrlAsync(item.PictureId);
                    model.MobilePictureUrl = await _pictureService.GetPictureUrlAsync(item.MobilePictureId);
                    return model;
                });
            });
            
            return await Task.FromResult(model);
        }
        public async Task<SliderItemModel> PrepareSliderItemModelAsync(int id)
        {
            var sliderItem = await _sliderService.GetSliderItemByIdAsync(id);
            var model = new SliderItemModel();
            if (sliderItem != null)
            {
                model = sliderItem.ToModel<SliderItemModel>();
                model.PictureUrl = await _pictureService.GetPictureUrlAsync(sliderItem.PictureId);
                model.MobilePictureUrl = await _pictureService.GetPictureUrlAsync(sliderItem.MobilePictureId);
            }

            return await Task.FromResult(model);
        }
        public async Task<SliderItem> PrepareSliderItemModelFromSliderItemAsync(SliderItemModel sliderItem)
        {
            var model = new SliderItem();
            if (sliderItem != null)
                model = sliderItem.ToEntity<SliderItem>();

            return await Task.FromResult(model);
        }
        public async Task<IList<SelectListItem>> GetSelectCategory()
        {
            var selectList = (await _categoryService.GetAllCategoriesAsync()).Select(s => new SelectListItem { Text = s.Name, Value = s.Id.ToString() }).ToList();
            //selectList.Insert(0, new SelectListItem { Text = "", Value = "0" });
            return selectList;
        }
        public async Task<IList<Category>> GetCategory(Category category = null)
        {
            if (category == null)
                return await _categoryService.GetAllCategoriesAsync();
            else
            {
                return (await _categoryService.GetAllCategoriesAsync()).Where(w => w.Name.ToLower() == category.Name.ToLower()).ToList();
            }
        }
        public async Task<IList<SelectListItem>> GetSelectManufacture()
        {
            var selectList = (await _manufactureService.GetAllManufacturersAsync()).Select(s => new SelectListItem { Text = s.Name, Value = s.Id.ToString() }).ToList();
            //selectList.Insert(0, new SelectListItem { Text = "", Value = "0" });
            return selectList;
        }
        public async Task<IList<Manufacturer>> GetMenufacture(Manufacturer manufacturer = null)
        {
            if (manufacturer == null)
                return await _manufactureService.GetAllManufacturersAsync();
            else
            {
                return (await _manufactureService.GetAllManufacturersAsync()).Where(w => w.Name.ToLower() == manufacturer.Name.ToLower()).ToList();
            }
        }
        public async Task<IList<SelectListItem>> GetSelectVendor()
        {
            var selectList = (await _vendorService.GetAllVendorsAsync()).Select(s => new SelectListItem { Text = s.Name, Value = s.Id.ToString() }).ToList();
            //selectList.Insert(0, new SelectListItem { Text = "", Value = "0" });
            return selectList;
        }
        public async Task<IList<Vendor>> GetVendor(Vendor vendor = null)
        {
            if (vendor == null)
                return await _vendorService.GetAllVendorsAsync();
            else
            {
                return (await _vendorService.GetAllVendorsAsync()).Where(w => w.Name.ToLower() == vendor.Name.ToLower()).ToList();
            }
        }
        public async Task<SliderModel> PrepareModelForEdit(Slider slider)
        {
            var model = await PrepareSliderModelAsync(slider);
            model.CatalogTypeList = Common.EnumToSelectedList();
            //prepare model customer roles
            await _aclSupportedModelFactory.PrepareModelCustomerRolesAsync(model, slider, false);
            //prepare model stores
            await _storeMappingSupportedModelFactory.PrepareModelStoresAsync(model, slider, false);

            var sliderEntity = await _sliderService.GetSliderEntityWithZoneBySliderIdAsync(slider.Id);
            List<string> entityNames = sliderEntity.Select(s => s.EntityName).Distinct().ToList();
            foreach (string s in entityNames)
            {
                model.CatalogTypeIds.Add(EnumHelper.GetEnum<CatalogEnum>(s));
            }
            model.EntityIds = sliderEntity.Select(s => s.EntityId).ToList();

            model.EntityStringIds = string.Empty;
            foreach (var eni in sliderEntity)
            {
                model.EntityStringIds += Common.GetEntityCharFromName(eni.EntityName) + "-" + eni.EntityId + ",";
            }

            model.WidgetZoneIds = sliderEntity.Select(s => s.WidgetZoneId).Distinct().ToList();

            model.SliderItemSearchModel = await PrepareSliderItemSearchModelAsync(new SliderItemSearchModel());

            return model;
        }
        public async Task<SliderModel> PrepareModelForCreate(SliderModel model)
        {
            model.CatalogTypeList = Common.EnumToSelectedList();
            model.CategoryList = new List<SelectListItem>();
            model.WidgetZoneItems = new List<SelectListItem>();
            model.TimeInterval = 5;
            model.IsActive = true;
            await _aclSupportedModelFactory.PrepareModelCustomerRolesAsync(model);
            await _storeMappingSupportedModelFactory.PrepareModelStoresAsync(model);

            return model;
        }
        private bool isThisZoneForThisEnity(int wzId, string entityName)
        {
            string widgetName = WidgetZoneHelper.GetCustomWidgetZone(wzId);
            return widgetName.Contains(entityName, StringComparison.OrdinalIgnoreCase);
        }
        public async Task SaveSliderEntity(List<CatalogModel> entity, IList<int> widgetZoneIds, int sliderId)
        {
            bool isEntitySave = false;
            foreach (var e in entity)
            {
                var sliderEntity = new SliderEntity();
                sliderEntity.EntityName = e.EntityName;
                sliderEntity.EntityId = e.EntityId;
                sliderEntity.SliderId = sliderId;
                isEntitySave = false;
                
                foreach (int zi in widgetZoneIds)
                {
                    bool flag = isThisZoneForThisEnity(zi, e.EntityName);
                    if (flag)
                    {
                        var existWidgetZone = await _sliderService.PublicSlideEntityCheckAsync(zi, e.EntityName, e.EntityId);
                        if (existWidgetZone == null)
                        {
                            if (!isEntitySave)
                            {
                                await _sliderService.InsertSliderEntityAsync(sliderEntity);
                                isEntitySave = true;
                            }
                            var sliderZone = new SliderZone();
                            sliderZone.WidgetZoneId = zi;
                            sliderZone.SliderEntityId = sliderEntity.Id;
                            await _sliderService.InsertSliderZoneAsync(sliderZone);
                        }
                    }
                }
            }
        }
        #endregion
    }
}
