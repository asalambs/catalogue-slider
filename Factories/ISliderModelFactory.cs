﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Vendors;
using Nop.Plugin.Widgets.CatalogueSlider.Domain;
using Nop.Plugin.Widgets.CatalogueSlider.Models;

namespace Nop.Plugin.Widgets.CatalogueSlider.Factories
{
    public interface ISliderModelFactory
    {
        Task<SliderModel> PrepareSliderModelAsync(Slider model, bool continuing = false);
        Task<SliderListModel> PrepareSliderListModelAsync(SliderSearchModel searchModel);
        Task<SliderSearchModel> PrepareSliderSearchModelAsync(SliderSearchModel searchModel);
        Task<SliderItemSearchModel> PrepareSliderItemSearchModelAsync(SliderItemSearchModel searchModel);
        //Task<Slider> SliderModelToSlider(SliderModel model);
        //Task<SliderListModel> PrepareSliderListModelAsync(SliderSearchModel searchModel);
        Task<SliderItemListModel> PrepareSliderItemListModelAsync(SliderItemSearchModel searchModel);
        Task<IList<SelectListItem>> PrepareCustomWidgetZonesAsync(bool withSpecialDefaultItem = true);
        Task<IList<SelectListItem>> GetSelectCategory();
        Task<IList<Category>> GetCategory(Category category = null);
        Task<IList<SelectListItem>> GetSelectManufacture();
        Task<IList<SelectListItem>> GetSelectVendor();
        Task<IList<Manufacturer>> GetMenufacture(Manufacturer manufacturer = null);
        Task<IList<Vendor>> GetVendor(Vendor vendor = null);
        Task<SliderItemModel> PrepareSliderItemModelAsync(int id);
        Task<SliderItem> PrepareSliderItemModelFromSliderItemAsync(SliderItemModel sliderItem);
        Task<SliderModel> PrepareModelForEdit(Slider slider);
        Task<SliderModel> PrepareModelForCreate(SliderModel model);
        Task SaveSliderEntity(List<CatalogModel> entity, IList<int> widgetZoneIds, int sliderId);
    }
}