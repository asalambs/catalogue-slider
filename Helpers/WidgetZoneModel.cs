﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Widgets.CatalogueSlider.Helpers
{
    public class WidgetZoneModel
    {
        public string Name { get; set; }
        public string AreaName { get; set; }
        public int Value { get; set; }
        public string PublicName { get; set; }
    }
}
