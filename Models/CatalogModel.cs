﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CatalogueSlider.Models
{
    public class CatalogModel
    {
        public string EntityName { get; set; }
        public int EntityId { get; set; }
    }
}
