﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CatalogueSlider.Models
{
    public class SliderItemLocalizedModel : ILocalizedLocaleModel
    {
        public int LanguageId { get; set; }
    }
}
