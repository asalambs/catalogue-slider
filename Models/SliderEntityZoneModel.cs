﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CatalogueSlider.Models
{
    public record SliderEntityZoneModel : SliderItemModel
    {
        public SliderEntityZoneModel()
        {
            WidgetZoneItems = new List<SelectListItem>();
            WidgetZoneIds = new List<int>();
            EntityIds = new List<int>();
        }
        #region Properties
        [NopResourceDisplayName("Plugins.Widgets.CatalogueSlider.Fields.CatalogId")]
        public IList<int> EntityIds { get; set; }
        public string EntityName { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.CatalogueSlider.Fields.WidgetZone")]
        public IList<int> WidgetZoneIds { get; set; }

        public IList<SelectListItem> WidgetZoneItems { get; set; }
        public int WidgetZoneId { get; set; }
        #endregion
    }
}
