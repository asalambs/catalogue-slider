﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Areas.Admin.Models.Catalog;
using Nop.Web.Areas.Admin.Models.Vendors;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Widgets.CatalogueSlider.Models
{
    public record SliderModel : SliderEntityZoneModel, IAclSupportedModel, IStoreMappingSupportedModel
    {
        #region Ctor
        public SliderModel()
        {
            Items = new List<SliderItemModel>();
            CatalogTypeList = new List<SelectListItem>();
            CategoryList = new List<SelectListItem>();
            CatalogTypeIds = new List<int>();
            AvailableStores = new List<SelectListItem>();
            SelectedStoreIds = new List<int>();
            AvailableCustomerRoles = new List<SelectListItem>();
            SelectedCustomerRoleIds = new List<int>();
        }
        #endregion
        #region Properties
        public string Name { get; set; }
        
        public IList<SliderItemModel> Items { get; set; }

        
        [NopResourceDisplayName("Plugins.Widgets.CatalogueSlider.Fields.TimeInterval")]
        public int TimeInterval { get; set; }

        public string TimeIntervalString { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.CatalogueSlider.Fields.WidgetZonePublicName")]
        public string WidgetZonePublicName { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.CatalogueSlider.Fields.IsActive")]
        public bool IsActive { get; set; }

        public IList<SelectListItem> CatalogTypeList { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.CatalogueSlider.Fields.CatalogType")]
        public IList<int> CatalogTypeIds { get; set; }

        public string EntityStringIds { get; set; }

        public IList<SelectListItem> CategoryList { get; set; }

       
        [NopResourceDisplayName("Plugins.Widgets.CatalogueSlider.Fields.StartTime")]
        [UIHint("DateTimeNullable")]
        public DateTime? StartTime { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.CatalogueSlider.Fields.EndTime")]
        [UIHint("DateTimeNullable")]
        public DateTime? EndTime { get; set; }


        //ACL (customer roles)
        [NopResourceDisplayName("Plugins.Widgets.CatalogueSlider.Fields.AclCustomerRoles")]
        public IList<int> SelectedCustomerRoleIds { get; set; }
        public IList<SelectListItem> AvailableCustomerRoles { get; set; }

        //store mapping
        [NopResourceDisplayName("Plugins.Widgets.CatalogueSlider.Fields.LimitedToStores")]
        public IList<int> SelectedStoreIds { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
        #endregion
    }
}
