﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CatalogueSlider.Models
{
    public enum CatalogEnum
    {
        Category = 1,
        Manufacture = 2,
        Vendor = 3
    }
}
