﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CatalogueSlider.Models
{
    public class SelectListItemCustom
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
}
