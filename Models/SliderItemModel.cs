﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CatalogueSlider.Models
{
    public record SliderItemModel: BaseNopEntityModel
    {
        #region Ctor
        public SliderItemModel()
        {
            SliderItemSearchModel = new SliderItemSearchModel();
        }
        #endregion
        #region Properties
        [NopResourceDisplayName("Plugins.Widgets.CatalogueSlider.Fields.PictureId")]
        [UIHint("Picture")]
        public int PictureId { get; set; }
        public string PictureUrl { get; set; }

        [NopResourceDisplayName("Plugins.Widgets.CatalogueSlider.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }
        public SliderItemSearchModel SliderItemSearchModel { get; set; }
        public int SliderId { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.CatalogueSlider.Fields.MobilePicture")]
        [UIHint("Picture")]
        public int MobilePictureId { get; set; }
        public string MobilePictureUrl { get; set; }
        #endregion
    }
}
