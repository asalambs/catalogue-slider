﻿using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Nop.Plugin.Widgets.CatalogueSlider.Models
{
    public partial record ConfigurationModel : BaseNopModel, ISettingsModel
    {
        #region Ctor
        public ConfigurationModel()
        {
        }
        #endregion
        #region Properties
        [NopResourceDisplayName("Plugins.Widgets.CatalogueSlider.Configuration.Fields.EnableSlider")]
        public bool EnableSlider { get; set; }
        public bool EnableSlider_OverrideForStore { get; set; }

        public int ActiveStoreScopeConfiguration { get; set; }
        #endregion
    }
}