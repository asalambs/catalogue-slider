﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Nop.Plugin.Widgets.CatalogueSlider.Models.SliderModel;

namespace Nop.Plugin.Widgets.CatalogueSlider.Models
{
    public record SliderListModel : BasePagedListModel<SliderModel>
    {
    }
    public record SliderItemListModel : BasePagedListModel<SliderItemModel>
    {
    }
    public record SliderEntityZoneListModel : BasePagedListModel<SliderEntityZoneModel>
    {
    }
}
