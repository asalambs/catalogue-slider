﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CatalogueSlider.Models
{
    public class Common
    {
        #region Methods
        public static List<SelectListItem> EnumToSelectedList()
        {
            var selectList = new List<SelectListItem>();
            selectList = Enum.GetValues(typeof(CatalogEnum)).Cast<CatalogEnum>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList();
            return selectList;
        }
        public static string GetEntityNameFromChar(string c)
        {
            string entityName;
            switch (c)
            {
                case "c":
                    entityName = "Category";
                    break;
                case "m":
                    entityName = "Manufacture";
                    break;
                default:
                    entityName = "Vendor";
                    break;
            }
            return entityName;
        }
        public static string GetEntityCharFromName(string name)
        {
            string entityName;
            switch (name)
            {
                case "Category":
                    entityName = "c";
                    break;
                case "Manufacture":
                    entityName = "m";
                    break;
                default:
                    entityName = "v";
                    break;
            }
            return entityName;
        }
        public static List<CatalogModel> GetCatalogModel(string entityStringIds)
        {
            if (string.IsNullOrEmpty(entityStringIds)) return null;
            var entity = new List<CatalogModel>();
            string[] stringIds = entityStringIds.Split(',');
            foreach (string s in stringIds)
            {
                string[] ids = s.Split('-');
                if (ids.Length > 1)
                {
                    int id = Convert.ToInt32(ids[1]);
                    var cat = new CatalogModel
                    {
                        EntityId = id,
                        EntityName = GetEntityNameFromChar(ids[0])
                    };
                    entity.Add(cat);
                }
            }

            return entity;
        }
        #endregion
    }
}
