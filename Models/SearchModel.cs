﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CatalogueSlider.Models
{
    public record SliderItemSearchModel : BaseSearchModel
    {
        public int SliderId { get; set; }
    }

    public record SliderSearchModel : BaseSearchModel
    {
        public SliderSearchModel()
        {
            AvailableActiveOptions = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Plugins.Widgets.CatalogueSlider.Fields.SearchActiveId")]
        public int SearchActiveId { get; set; }

        public IList<SelectListItem> AvailableActiveOptions { get; set; }
    }

    public record SliderEntityZoneSearchModel : BaseSearchModel
    {
        public int SliderId { get; set; }
    }
}
