﻿using Microsoft.Extensions.DependencyInjection;
using Nop.Core.Configuration;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Plugin.Widgets.CatalogueSlider.Factories;
using Nop.Plugin.Widgets.CatalogueSlider.Services;

namespace Nop.Plugin.Widgets.CatalogueSlider.Infrastructure
{
    public class DependencyRegistrar : IDependencyRegistrar
    {
        public void Register(IServiceCollection services, ITypeFinder typeFinder, AppSettings appSettings)
        {
            services.AddScoped<ISliderService, SliderService>();
            services.AddScoped<ISliderModelFactory, SliderModelFactory>();
        }

        public int Order => 1;
    }
}
