﻿using Nop.Core.Caching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CatalogueSlider.Infrastructure.Cache
{
    public static class SliderCache
    {
        static string cachePrefix = "Admin.Widget.Catalogue.Public.Component";
        public static CacheKey PublicComponentKey => new CacheKey(cachePrefix + "-{0}-{1}-{2}", PublicComponenPrefixCacheKey);
        public static string PublicComponenPrefixCacheKey => cachePrefix;

    }
}
