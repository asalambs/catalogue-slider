﻿using AutoMapper;
using Nop.Core.Infrastructure.Mapper;
using Nop.Plugin.Widgets.CatalogueSlider.Domain;
using Nop.Plugin.Widgets.CatalogueSlider.Models;

namespace Nop.Plugin.Widgets.CatalogueSlider.Infrastructure.Mapper
{
    /// <summary>
    /// Represents AutoMapper configuration for plugin models
    /// </summary>
    public class MapperConfiguration : Profile, IOrderedMapperProfile
    {
        #region Ctor

        public MapperConfiguration()
        {
            CreateMap<Slider, SliderModel>()
                .ForMember(entity => entity.EntityIds, options => options.Ignore());
            CreateMap<SliderModel, Slider>();
                //.ForMember(entity => entity.IsActive, options => options.MapFrom(x => x.IsActive));
                

            CreateMap<SliderItem, SliderItemModel>()
                .ForMember(entity => entity.PictureUrl, options => options.Ignore());
            CreateMap<SliderItemModel, SliderItem>();

            CreateMap<SliderEntity, SliderEntityZoneModel>();
            CreateMap<SliderEntityZoneModel, SliderEntity>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// Order of this mapper implementation
        /// </summary>
        public int Order => 1;

        #endregion
    }
}